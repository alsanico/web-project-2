﻿
# Web Project General Purpose Notes

## Project Origin
The whole structure template and original code were created using [HTML5 Boilerplate ](https://html5boilerplate.com/). 

## Project Purpose
This a project which I intend to use in order to get familiar with web development.


> Written with [StackEdit](https://stackedit.io/).
